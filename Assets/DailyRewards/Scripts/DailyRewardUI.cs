﻿/***************************************************************************\
Project:      Daily Rewards
Copyright (c) Niobium Studios.
Author:       Guilherme Nunes Barbosa (gnunesb@gmail.com)
\***************************************************************************/
using UnityEngine;
using UnityEngine.UI;

/* 
 * Daily Reward Object UI representation
 */
//namespace NiobiumStudios
//{
//    /** 
//     * The UI Representation of a Daily Reward.
//     * 
//     *  There are 3 states:
//     *  
//     *  1. Unclaimed and available:
//     *  - Shows the Color Claimed
//     *  
//     *  2. Unclaimed and Unavailable
//     *  - Shows the Color Default
//     *  
//     *  3. Claimed
//     *  - Shows the Color Claimed
//     *  
//     **/
    public class DailyRewardUI : MonoBehaviour
    {
        public bool showRewardName;

        [Header("UI Elements")]
        public Text textDay;                // Text containing the Day text eg. Day 12
        public Text textReward;             // The Text containing the Reward amount
        public Image imageBackground;
        public Image imageReward;           // The Reward Image
        public Image imageRewardUnclaimedAvailable;
        public Image imageRewardUnclaimedUnavailable;

        [Header("Internal")]
        public int day;

        [HideInInspector]
        public Reward reward;

        public DailyRewardState state;

        // The States a reward can have
        public enum DailyRewardState
        {
            UNCLAIMED_AVAILABLE,
            UNCLAIMED_UNAVAILABLE,
            CLAIMED
        }

        public void Initialize()
        {
            textDay.text = string.Format("Day {0}", day.ToString());
            if (reward.reward > 0)
            {
                if (showRewardName)
                {
                    textReward.text = reward.reward + " " + reward.unit;
                }
                else
                {
                    textReward.text = reward.reward.ToString();
                }
            }
            else
            {
                textReward.text = reward.unit.ToString();
            }
            imageReward.sprite = reward.sprite;
            imageRewardUnclaimedAvailable.sprite = reward.spriteUnclaimedAvailable;
            imageRewardUnclaimedUnavailable.sprite = reward.spriteUnclaimedUnAvailable;
        }

        // Refreshes the UI
        public void Refresh()
        {
            switch (state)
            {
                case DailyRewardState.UNCLAIMED_AVAILABLE:
                    imageReward.gameObject.SetActive(false);
                    imageRewardUnclaimedAvailable.gameObject.SetActive(true);
                    imageRewardUnclaimedUnavailable.gameObject.SetActive(false);
                    imageBackground.gameObject.SetActive(true);
                    break;
                case DailyRewardState.UNCLAIMED_UNAVAILABLE:
                    imageReward.gameObject.SetActive(false);
                    imageRewardUnclaimedAvailable.gameObject.SetActive(false);
                    imageRewardUnclaimedUnavailable.gameObject.SetActive(true);
                    imageBackground.gameObject.SetActive(false);

                    break;
                case DailyRewardState.CLAIMED:
                    imageReward.gameObject.SetActive(true);
                    imageRewardUnclaimedAvailable.gameObject.SetActive(false);
                    imageRewardUnclaimedUnavailable.gameObject.SetActive(false);
                    imageBackground.gameObject.SetActive(false);

                    break;
            }
        }
    }
//}