﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StopOnAccident : MonoBehaviour {

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.layer == 10 || other.gameObject.tag == "Player")
        {
            if (GetComponentInParent<CarPath>() != null)
            {
                //var speed = GetComponentInParent<CarPath>().SingleSpeed;
                GetComponentInParent<CarPath>().SingleSpeed = 0.01f;
                GetComponentInParent<CarPath>().OnValidate();
            }
        }
    }
}
