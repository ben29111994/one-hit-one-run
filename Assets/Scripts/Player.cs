﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
using MoreMountains.NiceVibrations;
using UnityStandardAssets.ImageEffects;
using UnityEngine.EventSystems;
using GameAnalyticsSDK;

public class Player : MonoBehaviour {
    public static Player playerInstance;
    public float powerValue = 0;
    public static float limitTime;
    public static float maxForce = 250;
    public Camera mainCamera;
    public ParticleSystem smoke;
    public Light policeLight;
    public GameObject winText;
    public GameObject[] turnPointsCoordinate;
    public GameObject[] carsList;
    public GameObject helicopterLight;
    public GameObject heliFlyPoint;
    public GameObject tutorialWaypoint;
    public Text currentLevel;
    public Text levelDistance;
    public Text currentPosText;
    public Image destinationPopup;
    public Image splashScreen;
    public Slider powerSlider;
    public Slider currentPos;
    public RectTransform mainCanvas;

    float touchStartTime = 0;
    bool isHold = false;
    bool canClick = true;
    bool isStop = false;
    bool isInFinishZone = false;
    bool isJudge = false;
    bool isDrift = false;
    Rigidbody rigid;
    Vector3 startPos;
    Vector3 previousPos;
    int currentTurnPoint;
    float currentDistance;
    float runDistance;
    public static float currentRot;
    Transform heliStart;
    Transform heliEnd;
    public static bool isCameraPan = true;
    static float totalDistance;
    static float totalTime;
    int currentScene;
    public static int isDailyMissionAvailable = 0;

    // Use this for initialization
    void Awake()
    {
        GameAnalytics.Initialize();
        startPos = transform.position;
        Application.targetFrameRate = 60;
    }

    void Start () {
        isDailyMissionAvailable = PlayerPrefs.GetInt("dailyMission");
        playerInstance = this;
        var currentCar = PlayerPrefs.GetInt("currentCar");
        var spawnCar = Instantiate(carsList[currentCar]);
        spawnCar.transform.parent = transform;
        spawnCar.transform.localPosition = Vector3.zero;
        spawnCar.transform.localEulerAngles = new Vector3(0, 180, 0);
        spawnCar.transform.localScale = new Vector3(1, 1, 1);
        transform.localScale = new Vector3(1, 1, 1);
        //Set destination popup
        float offsetPosZ = turnPointsCoordinate[turnPointsCoordinate.Length - 1].transform.position.z;
        float offsetPosY = turnPointsCoordinate[turnPointsCoordinate.Length - 1].transform.position.y;
        float offsetPosX = turnPointsCoordinate[turnPointsCoordinate.Length - 1].transform.position.x;
        var rot = mainCamera.transform.rotation.eulerAngles.y;
        if (rot < 0)
        {
            rot += 360;
        }
        if ((rot > 160 && rot < 200) || (rot < -160 && rot > -200))
        {
            offsetPosX = turnPointsCoordinate[turnPointsCoordinate.Length - 1].transform.position.x - 7.5f;
        }
        else if ((rot > -20 && rot < 20) || (rot > 340 && rot < 380))
        {
            offsetPosX = turnPointsCoordinate[turnPointsCoordinate.Length - 1].transform.position.x + 10;
        }
        else if (rot > 70 && rot < 110)
        {
            offsetPosZ = turnPointsCoordinate[turnPointsCoordinate.Length - 1].transform.position.z - 10;
        }
        else if (rot < 290 && rot > 250)
        {
            offsetPosZ = turnPointsCoordinate[turnPointsCoordinate.Length - 1].transform.position.z + 10;
        }
        Vector3 offsetPos = new Vector3(offsetPosX, offsetPosY, offsetPosZ);
        Vector2 canvasPos;
        Vector2 screenPoint = Camera.main.WorldToScreenPoint(offsetPos);
        RectTransformUtility.ScreenPointToLocalPointInRectangle(mainCanvas, screenPoint, null, out canvasPos);
        destinationPopup.transform.localPosition = canvasPos;
        destinationPopup.transform.localScale = new Vector3(0,0,0);
        destinationPopup.transform.DOScale(1, 0.5f);

        //PlayerPrefs.DeleteAll();
        var levelCheckpoint = PlayerPrefs.GetInt("currentLevel");
        currentScene = SceneManager.GetActiveScene().buildIndex;
        FacebookAnalytics.Instance.LogGame_startEvent(1);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "game", levelCheckpoint);
        if (currentScene < 50)
        {
            currentLevel.text = "LEVEL " + (levelCheckpoint + 1).ToString();
            //Set this off when testing, on when build
            if (currentScene != levelCheckpoint)
            {
                if (levelCheckpoint >= 50)
                {
                    var randomScene = Random.Range(0, 49);
                    SceneManager.LoadScene(randomScene);
                }
                else
                {
                    SceneManager.LoadScene(levelCheckpoint);
                }
            }

            heliStart = heliFlyPoint.transform.GetChild(0);
            heliEnd = heliFlyPoint.transform.GetChild(1);
            helicopterLight.GetComponent<AudioSource>().DOFade(0, 6);
            helicopterLight.transform.DOMove(heliEnd.position, 6);
        }
        else
        {
            maxForce = 750;
            currentLevel.text = "DAILY MISSION (You only have 2 chances)";
            transform.localScale = new Vector3(1.5f, 1.5f, 1.5f);
        }

        powerSlider.gameObject.SetActive(false);
        previousPos = transform.position;
        isCameraPan = true;
        SoundManager.instance.audioSource.volume = 1;
        currentRot = transform.rotation.eulerAngles.y;
        Time.timeScale = 1;
        isHold = false;
        canClick = true;
        isStop = false;
        isInFinishZone = false;
        isJudge = false;
        isDrift = false;
        powerSlider.maxValue = maxForce;
        splashScreen.color = new Color32(255, 255, 255, 255);
        splashScreen.DOFade(0, 1);

        //Calculate distance
        totalDistance = 0;
        totalTime = 0;
        currentTurnPoint = 0;
        currentDistance = 0;
        while (currentTurnPoint <= turnPointsCoordinate.Length - 2)
        {
            currentTurnPoint += 1;
            currentDistance = Vector3.Distance(turnPointsCoordinate[currentTurnPoint].transform.position, turnPointsCoordinate[currentTurnPoint - 1].transform.position);
            totalDistance += currentDistance;
            Debug.Log("Total Distance: " + totalDistance);
            var time = currentDistance / 30;
            if (currentDistance <= 30)
            {
                time = 1;
            }
            totalTime += time;
        }
        currentPos.maxValue = totalDistance;
        levelDistance.text = ((int)totalDistance).ToString();
        destinationPopup.GetComponentInChildren<Text>().text = "Destination " + ((int)totalDistance).ToString() + "m";

        limitTime = totalTime + 5;
        currentTurnPoint = 0;
        currentDistance = 0;
        runDistance = 0;
        rigid = GetComponentInChildren<Rigidbody>();
        policeLight.DOColor(new Color32(0, 17, 255, 255), 1f).SetLoops(-1, LoopType.Yoyo);
    }

    // Update is called once per frame
    void Update()
    {
        //transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, -10f, 10f), transform.position.z);
        if (!isStop)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0, currentRot, 0), 1 * Time.deltaTime);
        }
        if (!canClick || !isCameraPan)
        {
                mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, new Vector3(transform.position.x, 60, transform.position.z), 5 * Time.deltaTime);
        }
        //mainCamera.transform.rotation = Quaternion.Lerp(Camera.main.transform.rotation, Quaternion.Euler(0, 30, 0), Time.time);

        //Click on editor    
        if (Input.GetMouseButtonDown(0) && canClick)
        {
            if (!isCameraPan)
            {
                touchStartTime = Time.time;
                isHold = true;
            }
        }
        if (isHold)
        {
            powerValue = (Time.time - touchStartTime) * (maxForce / 2) * (1 / Time.timeScale);
            if (powerValue >= maxForce)
            {
                powerValue = maxForce;
                //isHold = false;
            }
            powerSlider.DOKill();
            if (powerValue >= powerSlider.value)
            {
                powerSlider.value = powerValue;
            }
        }
        if (Input.GetMouseButtonUp(0) && canClick && isHold)
        {
            if (!isCameraPan)
            {
                mainCamera.transform.DOKill();
                canClick = false;
                isHold = false;
                Run();
                powerSlider.DOKill();
                powerSlider.DOValue(0, 1);
                StartCoroutine(delayLimitTime(limitTime));
            }
        }
    }

    private void FixedUpdate()
    {
        currentPosText.text = ((int)currentPos.value).ToString();
        currentPos.value += Vector3.Distance(transform.position, previousPos);
        if (!canClick && !isHold && !isDrift && !isJudge)
        {
            if (Mathf.Abs(Vector3.Distance(transform.position, previousPos)) <= 0.1f)
            {
                var stopPos = transform.position;
                StartCoroutine(delayStop(stopPos));
            }
        }
        previousPos = transform.position; 
    }

    public void OnClickStartGame()
    {
        powerSlider.gameObject.SetActive(true);
        mainCamera.transform.DOKill();
        destinationPopup.gameObject.SetActive(false);
        tutorialWaypoint.SetActive(false);
        EventSystem.current.currentSelectedGameObject.gameObject.SetActive(false);
        isCameraPan = false;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.layer == 11)
        {
            SoundManager.instance.audioSource.DOKill();
            SoundManager.instance.audioSource.Stop();
            SoundManager.instance.audioSource.volume = 1;
            SoundManager.instance.PlaySound(SoundManager.instance.crash);
            var velocity = rigid.velocity;
            transform.DOKill();
            rigid.AddForce(velocity);
            isInFinishZone = false;
            MMVibrationManager.Vibrate();
        }
        if (other.gameObject.layer == 10)
        {
            SoundManager.instance.audioSource.DOKill();
            SoundManager.instance.audioSource.Stop();
            SoundManager.instance.audioSource.volume = 1;
            SoundManager.instance.PlaySound(SoundManager.instance.crash);
            SoundManager.instance.PlaySound(SoundManager.instance.run);
            StartCoroutine(delayStopEngine(1));
            MMVibrationManager.Vibrate();
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Finish")
        {
            other.GetComponent<Renderer>().material.DOColor(new Color32(255, 255, 255, 100), 0.75f);
            isInFinishZone = true;
            if (isStop)
            {
                if (!isJudge)
                {
                    Debug.Log("Win");
                    isJudge = true;
                    var score = Vector3.Distance(transform.position, other.transform.position);
                    if (Mathf.Abs(score) > 2 && Mathf.Abs(score) < 5)
                    {
                        winText.GetComponent<Text>().text = "PERFECT";
                        var perfect = PlayerPrefs.GetInt("perfect");
                        perfect++;
                        PlayerPrefs.SetInt("perfect", perfect);
                        switch (perfect)
                        {
                            case 3:
                                PlayerPrefs.SetInt(26.ToString(), 1);
                                DailyRewardsInterface.instance.panelReward.SetActive(true);
                                DailyRewardsInterface.instance.imageReward.sprite = SetupMyCarList.instance.carImages[26];
                                break;
                            case 5:
                                PlayerPrefs.SetInt(27.ToString(), 1);
                                DailyRewardsInterface.instance.panelReward.SetActive(true);
                                DailyRewardsInterface.instance.imageReward.sprite = SetupMyCarList.instance.carImages[27];
                                break;
                            case 7:
                                PlayerPrefs.SetInt(28.ToString(), 1);
                                DailyRewardsInterface.instance.panelReward.SetActive(true);
                                DailyRewardsInterface.instance.imageReward.sprite = SetupMyCarList.instance.carImages[28];
                                break;
                            case 9:
                                PlayerPrefs.SetInt(29.ToString(), 1);
                                DailyRewardsInterface.instance.panelReward.SetActive(true);
                                DailyRewardsInterface.instance.imageReward.sprite = SetupMyCarList.instance.carImages[29];
                                break;
                        }
                    }
                    else
                    {
                        winText.GetComponent<Text>().text = "GREAT";
                    }
                    StartCoroutine(delayWin());
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Turn Right")
        {
            if (!isDrift)
            {
                //canClick = true;
                SoundManager.instance.audioSource.DOKill();
                SoundManager.instance.audioSource.Stop();
                SoundManager.instance.audioSource.volume = 1;
                SoundManager.instance.PlaySound(SoundManager.instance.drift);
                SoundManager.instance.PlaySound(SoundManager.instance.slowMotion);
                StartCoroutine(delayDrift());
                Debug.Log("Turn Right");
                transform.DORotate(new Vector3(0, other.transform.rotation.eulerAngles.y, 0), 0.4f);
                currentRot = other.transform.rotation.eulerAngles.y;
                StartCoroutine(delayKeepRunning(0.6f));
            }
        }
        if (other.gameObject.tag == "Turn Left")
        {
            if (!isDrift)
            {
                //canClick = true;
                SoundManager.instance.audioSource.DOKill();
                SoundManager.instance.audioSource.Stop();
                SoundManager.instance.audioSource.volume = 1;
                SoundManager.instance.PlaySound(SoundManager.instance.drift);
                SoundManager.instance.PlaySound(SoundManager.instance.slowMotion);
                StartCoroutine(delayDrift());
                Debug.Log("Turn Left");
                transform.DORotate(new Vector3(0, other.transform.rotation.eulerAngles.y, 0), 0.4f);
                currentRot = other.transform.rotation.eulerAngles.y;
                StartCoroutine(delayKeepRunning(0.6f));
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Finish")
        {
            if (!isJudge)
            {
                Debug.Log("Loss miss FinishLine");
                isJudge = true;
                isInFinishZone = false;
                StartCoroutine(delayLoss());
            }
            other.GetComponent<Renderer>().material.DOColor(new Color32(255, 255, 255, 255), 0.75f);
        }
    }

    public void OnChangeCar()
    {
        var currentCar = PlayerPrefs.GetInt("currentCar");
        var spawnCar = Instantiate(carsList[currentCar]);
        spawnCar.transform.parent = transform;
        spawnCar.transform.localPosition = Vector3.zero;
        spawnCar.transform.localEulerAngles = new Vector3(0, 180, 0);
    }

    void Run()
    {
        if (currentTurnPoint < turnPointsCoordinate.Length - 2)
        {
            currentTurnPoint += 1;
            currentDistance = Vector3.Distance(turnPointsCoordinate[currentTurnPoint].transform.position, turnPointsCoordinate[currentTurnPoint - 1].transform.position);
            if(powerValue <= currentDistance)
            {
                currentDistance = powerValue;
            }
            Debug.Log("currentTurnPoint: " + currentTurnPoint);
            Debug.Log("currentDistance: " + currentDistance);
        }
        else
        {
            currentDistance = powerValue;
            Debug.Log("currentDistanceFixed: " + currentDistance);
        }
        powerValue -= currentDistance;
        if (powerValue < 0)
        {
            powerValue = 0;
        }
        var rot = transform.rotation.eulerAngles.y;
        if (rot < 0)
        {
            rot += 360;
        }
        Debug.Log("Rot: " + rot);
        float time;
        if (currentScene < 50)
        {
            time = currentDistance / 30;
            if (currentDistance <= 30)
            {
                time = 1;
            }
        }
        else
        {
            time = currentDistance / 50;
            if (currentDistance <= 50)
            {
                time = 1;
            }
        }
        if ((rot > 160 && rot < 200) || (rot < -160 && rot > -200))
        {
            transform.DOLocalMoveZ(transform.position.z + currentDistance, time);
        }
        else if ((rot > -20 && rot < 20) || (rot > 340 && rot < 380))
        {
            transform.DOLocalMoveZ(transform.position.z - currentDistance, time);
        }
        else if (rot > 70 && rot < 110)
        {
            transform.DOLocalMoveX(transform.position.x - currentDistance, time);
        }
        else if (rot < 290 && rot > 250)
        {
            transform.DOLocalMoveX(transform.position.x + currentDistance, time);
        }
        SoundManager.instance.audioSource.DOKill();
        SoundManager.instance.audioSource.Stop();
        SoundManager.instance.audioSource.volume = 1;
        SoundManager.instance.PlaySound(SoundManager.instance.run);
        StartCoroutine(delayStopEngine(time));
    }

    IEnumerator delayWin()
    {
        if (currentScene < 50)
        {
            helicopterLight.transform.DORotate(new Vector3(helicopterLight.transform.eulerAngles.x, helicopterLight.transform.eulerAngles.y + 180, helicopterLight.transform.eulerAngles.z), 2);
            helicopterLight.GetComponent<AudioSource>().DOFade(1, 3);
            helicopterLight.transform.DOMove(heliStart.position, 6);
            yield return new WaitForSeconds(2);
        }
        winText.SetActive(true);
        winText.GetComponent<Text>().DOFade(1, 0.5f);
        mainCamera.GetComponent<BlurOptimized>().enabled = true;
        StartCoroutine(delayLoadScene());
    }

    IEnumerator delayLoss()
    {
        if (currentScene < 50)
        {
            helicopterLight.transform.DORotate(new Vector3(helicopterLight.transform.eulerAngles.x, helicopterLight.transform.eulerAngles.y + 180, helicopterLight.transform.eulerAngles.z), 2);
            helicopterLight.GetComponent<AudioSource>().DOFade(1, 3);
            helicopterLight.transform.DOMove(new Vector3(transform.position.x, helicopterLight.transform.position.y, transform.position.z), 3);
            yield return new WaitForSeconds(2);
        }
        winText.SetActive(true);
        winText.GetComponent<Text>().DOFade(1, 0.5f);
        mainCamera.GetComponent<BlurOptimized>().enabled = true;
        winText.GetComponent<Text>().text = "LOSER!";
        StartCoroutine(delayLoadScene());
    }

    IEnumerator delayStopEngine(float time)
    {
        SoundManager.instance.audioSource.volume = 1;
        yield return new WaitForSeconds(time / 2);
        SoundManager.instance.audioSource.DOFade(0, time / 2);
        yield return new WaitForSeconds(time / 2);
        SoundManager.instance.audioSource.Stop();
    }

    IEnumerator delayKeepRunning(float time)
    {
        yield return new WaitForSeconds(time);
        isDrift = false;
        Run();
    }

    IEnumerator delayLoadScene()
    {
        var levelCheckpoint = PlayerPrefs.GetInt("currentLevel");
        var currentScene = SceneManager.GetActiveScene().buildIndex;
        var missionStatus = PlayerPrefs.GetInt("dailyMission");
        SoundManager.instance.audioSource.DOKill();
        SoundManager.instance.audioSource.volume = 1;
        if (winText.GetComponent<Text>().text != "LOSER!")
        {
            if (currentScene >= 50)
            {
                bool isUnlocked = false;
                while(!isUnlocked)
                {
                    var randomUnlockCar = Random.Range(1, 33);
                    var checkUnlocked = PlayerPrefs.GetInt(randomUnlockCar.ToString());
                    if(checkUnlocked == 0)
                    {
                        PlayerPrefs.SetInt(randomUnlockCar.ToString(), 1);
                        DailyRewardsInterface.instance.panelReward.SetActive(true);
                        DailyRewardsInterface.instance.imageReward.sprite = SetupMyCarList.instance.carImages[randomUnlockCar];
                        missionStatus += 2;
                        PlayerPrefs.SetInt("dailyMission", missionStatus);
                        isUnlocked = true;
                    }
                }
            }
            else
            {
                levelCheckpoint++;
                var combo = PlayerPrefs.GetInt("combo");
                combo++;
                PlayerPrefs.SetInt("combo", combo);
                switch (combo)
                {
                    case 1:
                        PlayerPrefs.SetInt(30.ToString(), 1);
                        DailyRewardsInterface.instance.panelReward.SetActive(true);
                        DailyRewardsInterface.instance.imageReward.sprite = SetupMyCarList.instance.carImages[30];
                        break;
                    case 2:
                        PlayerPrefs.SetInt(31.ToString(), 1);
                        DailyRewardsInterface.instance.panelReward.SetActive(true);
                        DailyRewardsInterface.instance.imageReward.sprite = SetupMyCarList.instance.carImages[31];
                        break;
                    case 3:
                        PlayerPrefs.SetInt(32.ToString(), 1);
                        DailyRewardsInterface.instance.panelReward.SetActive(true);
                        DailyRewardsInterface.instance.imageReward.sprite = SetupMyCarList.instance.carImages[32];
                        break;
                    case 4:
                        PlayerPrefs.SetInt(33.ToString(), 1);
                        DailyRewardsInterface.instance.panelReward.SetActive(true);
                        DailyRewardsInterface.instance.imageReward.sprite = SetupMyCarList.instance.carImages[33];
                        break;
                }
            }
            SoundManager.instance.PlaySound(SoundManager.instance.win);
        }
        else
        {
            var combo = PlayerPrefs.GetInt("combo");
            combo--;
            PlayerPrefs.SetInt("combo", combo);
            if (currentScene >= 50)
            {
                missionStatus += 1;
                PlayerPrefs.SetInt("dailyMission", missionStatus);
            }
            SoundManager.instance.PlaySound(SoundManager.instance.loss);
        }
        yield return new WaitForSeconds(2);
        if (currentScene >= 50 && missionStatus < 3)
        {
            SceneManager.LoadScene(currentScene);
        }
        else
        {
            PlayerPrefs.SetInt("currentLevel", levelCheckpoint);
            if (levelCheckpoint >= 50)
            {
                var randomScene = Random.Range(0, 49);
                SceneManager.LoadScene(randomScene);
            }
            else
                SceneManager.LoadScene(levelCheckpoint);
        }
        FacebookAnalytics.Instance.LogGame_endEvent(1);
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "game", levelCheckpoint);
    }

    IEnumerator delayDrift()
    {
        smoke.Play();
        isDrift = true;
        Time.timeScale = 0.2f;
        //Camera.main.transform.DOMoveX(transform.position.x, 0.3f).SetLoops(2, LoopType.Yoyo);
        //Camera.main.transform.DOMoveY(40, 0.3f).SetLoops(2, LoopType.Yoyo);
        yield return new WaitForSeconds(0.29f);
        Time.timeScale = 1f;
        smoke.Stop();
    }

    IEnumerator delayStop(Vector3 stopPos)
    {
        yield return new WaitForSeconds(1f);
        if (Mathf.Abs(Vector3.Distance(transform.position, stopPos)) <= 0.1f)
        {
            if (!isJudge)
            {
                isStop = true;
                if (!isInFinishZone)
                {
                    Debug.Log("Loss stop");
                    isJudge = true;
                    StartCoroutine(delayLoss());
                }
            }
        }
    }

    IEnumerator delayLimitTime(float time)
    {
        yield return new WaitForSeconds(time);
        if (!isJudge)
        {
            isInFinishZone = false;
            Debug.Log("Loss out of time");
            isJudge = true;
            StartCoroutine(delayLoss());
        }
    }
}
