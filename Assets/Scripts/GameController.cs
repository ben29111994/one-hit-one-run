﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour {
    public static GameController instance;
    
    [Header("Info Map")]
    public int myMap;
    public GameObject startObject;
    public GameObject endObject;
    public GameObject player;

    private List<GameObject> maps = new List<GameObject>();
    private int countMap;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        player.transform.position = startObject.transform.position;
        Player.currentRot = startObject.transform.localEulerAngles.y;
        player.transform.localEulerAngles = new Vector3(0, startObject.transform.localEulerAngles.y, 0);
        //AddMapToList();
        //RandomMap();
    }

    private void AddMapToList()
    {
        countMap = transform.GetChild(0).transform.childCount;
        for(int i = 0; i< countMap; i++)
        {
            maps.Add(transform.GetChild(0).GetChild(i).gameObject);
        }
    }

    private void RandomMap()
    {
        myMap = Random.Range(0, countMap);
        for(int i = 0; i < countMap; i++)
        {
            if(i == myMap)
            {
                maps[i].SetActive(true);
                startObject = maps[i].transform.GetChild(0).gameObject;
                endObject = maps[i].transform.GetChild(1).gameObject;
                player.transform.position = startObject.transform.position;
                player.transform.localEulerAngles = new Vector3(0, startObject.transform.localEulerAngles.y, 0);
            }
            else
            {
                maps[i].SetActive(false);
            }
        }
    }


}
