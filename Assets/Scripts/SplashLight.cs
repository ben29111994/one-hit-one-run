﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SplashLight : MonoBehaviour {

    // Use this for initialization
    void Start() {
        if (transform.gameObject.layer == 5)
        {
            var text = GetComponent<Text>().DOFade(0, 1).SetLoops(-1, LoopType.Yoyo);
        }
        else
        {
            var light = GetComponent<Light>().DOIntensity(0, 1).SetLoops(-1, LoopType.Yoyo);
        }
	}
}
