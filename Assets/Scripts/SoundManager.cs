﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

	public static SoundManager instance;

	public AudioSource audioSource;
	public AudioClip run, buttonClick, drift, slowMotion, crash, win, loss;	



	void Awake(){
        if (instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
		audioSource = GetComponent<AudioSource> ();
	}

	public void PlaySound(AudioClip clip){
		audioSource.PlayOneShot(clip);
	}
}
