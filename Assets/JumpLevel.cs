﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class JumpLevel : MonoBehaviour {
    public Text inputText;

    public void OnChangeLevel()
    {
        PlayerPrefs.SetInt("currentLevel", int.Parse(inputText.text));
        SceneManager.LoadScene(int.Parse(inputText.text));
    }
}
