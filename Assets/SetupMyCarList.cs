﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetupMyCarList : MonoBehaviour {
    public static SetupMyCarList instance;
    public GameObject carModelPrefab;
    GameObject carModePivot;
    public Sprite[] carImages;

    private void Start()
    {
        instance = this;
    }

    private void OnEnable()
    {
        for (int i = 0; i <= 40; i++)
        {
            var isUnlocked = PlayerPrefs.GetInt(i.ToString());
            if (isUnlocked == 1 || i == 0)
            {
                carModePivot = Instantiate(carModelPrefab) as GameObject;
                carModePivot.transform.SetParent(transform);
                carModePivot.transform.localScale = new Vector3(1, 1, 1);
                carModePivot.name = i.ToString();
                carModePivot.GetComponent<Image>().sprite = carImages[i];
            }
        }
    }

    private void OnDisable()
    {
        foreach (Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }
}
