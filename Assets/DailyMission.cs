﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DailyMission : MonoBehaviour {
    Animator animator;
    static bool isHide = false;

    // Use this for initialization
    void Start()
    {
        var isDailyMissionAvailable = PlayerPrefs.GetInt("dailyMission");
        if(isDailyMissionAvailable == 0 || isDailyMissionAvailable > 2)
        {
            var dailyMissionButton = transform.GetChild(1);
            dailyMissionButton.GetComponent<Button>().interactable = false;
        }
        isHide = false;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!Player.isCameraPan && !isHide)
        {
            Debug.Log("isHide");
            isHide = true;
            animator.Play("LeftButtons_Hide");
        }
    }
    public void OnLoadDailyMap()
    {
        var randomDailyMission = Random.Range(50, 56);
        SceneManager.LoadScene(randomDailyMission);
    }
}
