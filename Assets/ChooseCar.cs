﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseCar : MonoBehaviour {

    public void OnClickChooseCar()
    {
        PlayerPrefs.SetInt("currentCar", int.Parse(gameObject.name));
        Player.playerInstance.OnChangeCar();
    }
}
